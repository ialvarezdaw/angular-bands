import { AngularBandsPage } from './app.po';

describe('angular-bands App', () => {
	let page: AngularBandsPage;

	beforeEach(() => {
		page = new AngularBandsPage();
	});

	it('should display message saying app works', () => {
		page.navigateTo();
		expect(page.getParagraphText()).toEqual('app works!');
	});
});
