# AngularBands

Aplicación para un jefe nostálgico! :D

### Quick start
**Realizado con @angular/cli: 1.0.0-rc.1**

```bash
# Clonar el repositorio.
git clone https://gitlab.com/ialvarezdaw/angular-bands.git

# Instalar las dependencias
npm install

# Lanzar app en local.
ng serve || npm run start

# Lanzar los test unitarios.
ng test || npm run test

# Lanzar test end to end.
ng e2e || npm run e2e
```
Ir a [http://localhost:4200](http://localhost:4200) en tu navegador.