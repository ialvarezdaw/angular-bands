import { Injectable } from '@angular/core';
import { URLSearchParams, Jsonp } from '@angular/http';
import 'rxjs/add/operator/map';
import { WikiPedia } from './wikipedia.model';
import { Observable } from 'rxjs';

@Injectable()
export class WikipediaService {

    constructor(
       private jsonp: Jsonp
    ) {}

    /**
     * Retrieve wiki info.
     * @param page
     */
    getExtracts(page: string): Observable<WikiPedia> {
        page = this.formatPage(page);
        let search = new URLSearchParams();
        search.set('format', 'json');
        search.set('action', 'query');
        search.set('prop', 'extracts');
        search.set('explaintext', '1');
        search.set('exchars', '156');
        search.set('titles', page);

        return this.jsonp
            .get('http://es.wikipedia.org/w/api.php?callback=JSONP_CALLBACK', { search })
            .map((res) => {
                let pages = res.json().query.pages;
                return new WikiPedia(pages[Object.keys(pages)[0]]);
            });
    }

    /**
     * Format param page to call wiki api.
     * @param str
     * @returns {string}
     */
    private formatPage(str): string {
        str = str.replace('/', '');
        return this.firstWordUpperCase(str.replace(/-/g, ' '));
    }

    /**
     * Regex to uppercase each words in string.
     * @param str
     * @returns {void|string}
     */
    private firstWordUpperCase(str): string {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
}
