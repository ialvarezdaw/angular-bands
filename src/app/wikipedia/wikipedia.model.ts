export class WikiPedia {
	public pageid: number;
	public ns: number;
	public title: string;
	public extract: string;

	constructor(json?: any) {
		if(!json) {
			return;
		}
		this.pageid = json.pageid;
		this.ns = json.ns;
		this.title = json.title;
		this.extract = json.extract;
	}
}
