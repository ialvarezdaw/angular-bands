import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import { WikipediaService } from './wikipedia.service';
import { Jsonp, BaseRequestOptions, ResponseOptions, Response } from '@angular/http';
import { BrowserJsonp } from '@angular/http/src/backends/browser_jsonp';
import { MockBackend } from '@angular/http/testing';

let service: WikipediaService;
let mockBack: MockBackend;

describe('Wikipedia Service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                WikipediaService,
                BrowserJsonp,
                MockBackend,
                BaseRequestOptions,
                {
                    provide: Jsonp,
                    useFactory: (backend, options) => new Jsonp(backend, options),
                    deps: [MockBackend, BaseRequestOptions]
                }
            ]
        });

		mockBack = TestBed.get(MockBackend);
        service = TestBed.get(WikipediaService);
    });

    it('Retrieve wikipedia page info', fakeAsync(() => {
        let res: any;
		mockBack.connections.subscribe((c: any) => {
            expect(c.request.url).toBe('http://es.wikipedia.org/w/api.php?callback=JSONP_CALLBACK&format=json&' +
                'action=query&prop=extracts&explaintext=1&exchars=156&titles=Queen');
            let response = new ResponseOptions({
                body: JSON.stringify({
                    'query': {
                        'pages': {
                            '7958': {
                                'title': 'wiki title response',
                                'extract': 'Extracto de una página de wikipedia'
                    }}}}),
                status: 200
            });
            c.mockRespond(new Response(response));
        });

        service.getExtracts('/queen').subscribe((_res) => {
            res = _res;
        });
        tick();
        expect(res.title).toEqual('wiki title response');
        expect(res.extract).toEqual('Extracto de una página de wikipedia');
    }));
});
