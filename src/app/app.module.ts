import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { BandsComponent } from './bands/bands.component';
import { FooComponent } from './foo/foo.component';
import { BarComponent } from './bar/bar.component';
import { YoutubeService } from './youtube/youtube.service';
import { WikipediaService } from './wikipedia/wikipedia.service';
import { SeoService } from './seo/seo.service';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		BandsComponent,
		FooComponent,
		BarComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		AppRoutingModule,
		RouterModule,
		JsonpModule
	],
	providers: [
		YoutubeService,
		WikipediaService,
		SeoService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
