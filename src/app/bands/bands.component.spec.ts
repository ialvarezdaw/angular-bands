import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BandsComponent } from './bands.component';
import { YoutubeService } from '../youtube/youtube.service';
import { WikipediaService } from '../wikipedia/wikipedia.service';
import { MockWikipediaService } from '../wikipedia/wikipedia.service.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { SeoService } from '../seo/seo.service';

describe('BandsComponent', () => {
	let component: BandsComponent;
	let fixture: ComponentFixture<BandsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ BandsComponent ],
			providers: [
				YoutubeService,
				SeoService,
				{
					provide: WikipediaService, useClass: MockWikipediaService
				}
			],
			imports: [
				RouterTestingModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(BandsComponent);
		component = fixture.componentInstance;
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
