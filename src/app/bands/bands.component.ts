import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../youtube/youtube.service';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { WikipediaService } from '../wikipedia/wikipedia.service';
import { WikiPedia } from '../wikipedia/wikipedia.model';
import { SeoService } from '../seo/seo.service';

@Component({
	selector: 'app-bands',
	templateUrl: './bands.component.html',
	styleUrls: ['./bands.component.css']
})
export class BandsComponent implements OnInit {

	idVideo: string;
	wikiObject: WikiPedia;

	constructor(
        private youtubeService: YoutubeService,
		private wikiService: WikipediaService,
		private router: Router,
		private sanitizer: DomSanitizer,
		private seoService: SeoService
	) {}

	ngOnInit() {
		this.idVideo = this.youtubeService.getIdVideo(this.router.url);
		this.getWikiInfo();
	}

	/**
	* Return src attribute to youtube iframe.
	* @returns {string}
	*/
	getUrlVideo(): SafeResourceUrl {
		return this.sanitizer.bypassSecurityTrustResourceUrl('http://www.youtube.com/embed/'+this.idVideo+'?frameborder="0"');
	}

	/**
	* Populate data in wikiObject.
	*/
	getWikiInfo(): void {
		this.wikiService.getExtracts(this.router.url)
			.subscribe(
				(data: WikiPedia) => {
					this.wikiObject = data;
					this.seoService.setTitle(this.wikiObject.title);
					this.seoService.setMetaDescription(this.wikiObject.extract);
					this.seoService.setRandomMetaKeywords(this.wikiObject.extract);
			}
		);
	}
}
