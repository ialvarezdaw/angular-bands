import { Component } from '@angular/core';
import { title, description, keywords } from '../seo/seo';
import { SeoService } from '../seo/seo.service';

@Component({
	selector: 'app-bar',
	templateUrl: './bar.component.html'
})
export class BarComponent {

	constructor(
		private seoservice: SeoService
	) {
		this.seoservice.setTitle(title);
		this.seoservice.setMetaDescription(description);
		this.seoservice.setMetaKeywords(keywords);
	}

}
