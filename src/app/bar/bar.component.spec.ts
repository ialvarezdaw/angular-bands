import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BarComponent } from './bar.component';
import { By } from '@angular/platform-browser';
import { SeoService } from '../seo/seo.service';

describe('BarComponent', () => {
    let component: BarComponent;
    let fixture: ComponentFixture<BarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ BarComponent ],
            providers: [ SeoService ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should render a p element with "En construcción"', () => {
        const compiled = fixture.debugElement.query(By.css('p'));
        expect(compiled.nativeElement.textContent.trim()).toEqual('En construcción');
    });
});
