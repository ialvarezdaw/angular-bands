import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FooComponent } from './foo.component';
import { By } from '@angular/platform-browser';
import { SeoService } from '../seo/seo.service';

describe('FooComponent', () => {
	let component: FooComponent;
	let fixture: ComponentFixture<FooComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ FooComponent ],
			providers: [ SeoService ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FooComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should render a p element with "En construcción"', () => {
		const compiled = fixture.debugElement.query(By.css('p'));
		expect(compiled.nativeElement.textContent.trim()).toEqual('En construcción');
	});
});
