import { Component } from '@angular/core';
import { title, description, keywords } from '../seo/seo';
import { SeoService } from '../seo/seo.service';

@Component({
  selector: 'app-foo',
  templateUrl: './foo.component.html'
})
export class FooComponent {

	constructor(
		private seoservice: SeoService
	) {
		this.seoservice.setTitle(title);
		this.seoservice.setMetaDescription(description);
		this.seoservice.setMetaKeywords(keywords);
	}
}
