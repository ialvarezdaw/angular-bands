import { Injectable } from '@angular/core';
import { URLS } from '../../config/urls';

@Injectable()
export class YoutubeService {

	getIdVideo(currentPath): string {
		let id: string;
		currentPath = currentPath.replace('/', '');
		switch (currentPath) {
			case URLS.ROLLING_STONES:
				id = 'lrIjMzBr-ck';
				break;
			case URLS.THE_BEATLES:
				id = 'NCtzkaL2t_Y';
				break;
			case URLS.QUEEN:
				id = 'HgzGwKwLmgM';
				break;
			default:
				id = 'HgzGwKwLmgM';
				break;
		}
		return id;
	}
}
