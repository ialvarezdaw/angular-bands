import { TestBed, inject } from '@angular/core/testing';
import { YoutubeService } from './youtube.service';

let comp: YoutubeService;
describe('Youtube Service', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				YoutubeService
			]
		});
	});

	beforeEach(inject([YoutubeService], (youtubeService: YoutubeService) => {
		comp = youtubeService;
	}));

	it('Should return id video from The beatles', () => {
		expect(comp.getIdVideo('/the-beatles')).toEqual('NCtzkaL2t_Y');
	});

	it('Should return id video from The Rolling Stones', () => {
		expect(comp.getIdVideo('/the-rolling-stones')).toEqual('lrIjMzBr-ck');
	});

	it('Should return id video from Queen', () => {
		expect(comp.getIdVideo('/queen')).toEqual('HgzGwKwLmgM');
	});

	it('Should return a value by default (queen id)', () => {
		expect(comp.getIdVideo('/kubide')).toEqual('HgzGwKwLmgM');
	});
});
