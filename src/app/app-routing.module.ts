import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FooComponent } from './foo/foo.component';
import { BarComponent } from './bar/bar.component';
import { BandsComponent } from './bands/bands.component';
import { URLS } from '../config/urls';

const routerConfig: Routes = [
	{path: '', component: HomeComponent},
	{path: URLS.FOO, component: FooComponent},
	{path: URLS.BAR, component: BarComponent},
	{path: URLS.ROLLING_STONES, component: BandsComponent},
	{path: URLS.THE_BEATLES, component: BandsComponent},
	{path: URLS.QUEEN, component: BandsComponent},
	{path: '**', component: HomeComponent}
];

@NgModule({
	imports: [
        RouterModule.forRoot(routerConfig)
	],
	exports: [
        RouterModule
	]
})
export class AppRoutingModule {}
