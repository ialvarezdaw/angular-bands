import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { getDOM } from '@angular/platform-browser/src/dom/dom_adapter';

@Injectable()
export class SeoService {
	/**
	 * Angular 2 Title Service
	 */
	private titleService: Title;
	/**
	 * <head> Element of the HTML document
	 */
	private headElement: HTMLElement;
	private metaDescription: HTMLElement;
	private metaKeywords: HTMLElement;
	private DOM: any;

	/**
	 * Inject the Angular 2 Title Service
	 * @param titleService
	 */
	constructor(titleService: Title) {
		this.titleService = titleService;
		this.DOM = getDOM();

		/**
		 * get the <head> Element
		 * @type {any}
		 */
		this.headElement = this.DOM.query('head');
		this.metaDescription = this.getOrCreateMetaElement('description');
		this.metaKeywords = this.getOrCreateMetaElement('keywords');
	}

	public setTitle(newTitle: string) {
		this.titleService.setTitle(newTitle);
	}

	public setMetaDescription(description: string): void {
		this.metaDescription.setAttribute('content', description);
	}

	public setMetaKeywords(keywords: string): void {
		this.metaKeywords.setAttribute('content', keywords);
	}

	public setRandomMetaKeywords(keywords: string): void {
		let str = '';
		// Retrieve the first fifty words from string.
		let keys = keywords.split(' ', 50);
		// Shuffle array to get random words.
		keys = this.shuffle(keys);
		let i = 5;
		do {
			// Clean words and create the string to set in Keywords.
			str += keys[i].replace(/\...|,/g, '') + ', ';
			i--;
		} while( i >= 0 );
		str = str.substr(0, str.length-2).trim();
		this.metaKeywords.setAttribute('content', str);
	}

	/**
	 * Shuffle an array.
	 * @param array
	 * @returns {any}
	 */
	private shuffle( array: string[] ): string[] {
		let m = array.length, t, i;
		// While there remain elements to shuffle…
		while ( m ) {
			// Pick a remaining element…
			i = Math.floor(Math.random() * m--);
			// And swap it with the current element.
			t = array[m];
			array[m] = array[i];
			array[i] = t;
		}
		return array;
	}
	/**
	 * get the HTML Element when it is in the markup, or create it.
	 * @param name
	 * @returns {HTMLElement}
	 */
	private getOrCreateMetaElement(name: string): HTMLElement {
		let el: HTMLElement;
		el = this.DOM.query('meta[name=' + name + ']');
		if (el === null) {
			el = this.DOM.createElement('meta');
			el.setAttribute('name', name);
			this.headElement.appendChild(el);
		}
		return el;
	}
}
