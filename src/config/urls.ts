export const URLS = {
    FOO: 'foo',
    BAR: 'bar',
    ROLLING_STONES: 'the-rolling-stones',
    THE_BEATLES: 'the-beatles',
    QUEEN: 'queen'
};
